/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import type {PropsWithChildren} from 'react';
import {
  Dimensions,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

type SectionProps = PropsWithChildren<{
  title: string;
}>;

const {width, height} = Dimensions.get('window');
function App(): JSX.Element {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };
let text ="セッションの有効期限が切れました。\n再度ログインしてください。"
  return (
    <SafeAreaView style={backgroundStyle}>
      <View
        style={{flex: 1, alignItems: 'center', height: height, width: width}}>
        <View
          style={{
            height: height * 0.2,
            marginTop: 101,
            width: width * 0.75,
            borderRadius: 10,
            borderWidth: 1,
            backgroundColor: '#fff',
            borderColor: '#fff',
            shadowColor: '#000',
            shadowOffset: {
              width: 0,
              height: 12,
            },
            shadowOpacity: 0.58,
            shadowRadius: 16.0,

            elevation: 24,
          }}>
          <View style={{flex: 2,alignItems:"center",justifyContent:"center"}} >
            <Text style ={{alignItems:"center",textAlign:"center",marginHorizontal:15,fontSize:15,color:"#262626"}} >
            {text}
            </Text>
          </View>
          <View
            style={{
              flex: 1,
              alignItems: 'center',
              justifyContent: 'center',
              borderTopWidth: 0.75,
            }}>
            <TouchableOpacity>
              <Text style={{fontSize: 18, fontWeight: '600', color: '#0984e3'}}>
                OK
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
